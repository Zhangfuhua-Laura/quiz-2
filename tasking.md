## add and get Rob
* add Rob 
* add Rob with constraint

* get specific staff successfully
* get 404 if staffId is not exist
* get all staffs in order

## set Rob timezone

* add correct zoneId
* add incorrect zoneId

## get timezone list
* get staff info with zoneId
* get zone id ordered by character

## reserve Rob
* add correct reservation
* add incorrect reservation content
* get reservation list
* fail to reservation


