package com.twuc.webApp;

import com.twuc.webApp.entity.Staff;
import com.twuc.webApp.entity.ZoneId;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.time.zone.ZoneRulesProvider;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TimeZoneTest extends ApiTestBase {
    @Test
    void should_update_staff_zoneId() throws Exception {

        addStaff();

        mockMvc.perform(put("/api/staffs/1/timezone")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new ZoneId("Asia/Chongqing"))))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_400_when_add_incorrect_zoneId() throws Exception {

        addStaff();

        mockMvc.perform(put("/api/staffs/1/timezone")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize("Asia/incorrect")))
                .andExpect(status().is(400));
    }

    @Test
    void should_get_all_staffs_ordered_by_id() throws Exception {
        String serializedAllStaff =
                "[{\"id\":1,\"firstName\":\"Rob\",\"lastName\":\"Hall\",\"timeZone\":\"Asia/Chongqing\"}," +
                        "{\"id\":2,\"firstName\":\"Rob\",\"lastName\":\"Hall\",\"timeZone\":null}]";

        addStaff();
        updateZoneId();
        addStaff();

        mockMvc.perform(get("/api/staffs"))
                .andExpect(status().isOk())
                .andExpect(content().string(serializedAllStaff));
    }

    @Test
    void should_get_all_zoneId_ordered_by() throws Exception {
        mockMvc.perform(get("/api/timezones"))
                .andExpect(status().isOk())
                .andExpect(content().string(serialize(ZoneRulesProvider.getAvailableZoneIds())));
    }

    private void addStaff() throws Exception {
        mockMvc.perform(
                post("/api/staffs")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(serialize(new Staff("Rob", "Hall"))));
    }

    private void updateZoneId() throws Exception {
        mockMvc.perform(put("/api/staffs/1/timezone")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new ZoneId("Asia/Chongqing"))));
    }
}
