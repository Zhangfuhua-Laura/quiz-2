package com.twuc.webApp.entity;

public class TimeZone {
    private String zoneId;

    public TimeZone() {
    }

    public TimeZone(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }
}
