package com.twuc.webApp;

import com.twuc.webApp.entity.Staff;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class AddAndGetStaffTest extends ApiTestBase {

    @Test
    void should_add_rob_successfully() throws Exception {
        mockMvc.perform(
                post("/api/staffs")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(serialize(new Staff("Rob", "Hall"))))
                .andExpect(status().is(201))
                .andExpect(header().string("location", "/api/staffs/1"));
    }

    @Test
    void should_return_400_when_name_is_too_long() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new Staff("tooLoooooooooooooooooooooooooooooooooooooooooooooooooooooooooooog", "lastName")))
        ).andExpect(status().is(400));
    }

    @Test
    void should_get_specific_staff_given_correct_id() throws Exception {
        addStaff();

        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.firstName").value("Rob"))
                .andExpect(jsonPath("$.lastName").value("Hall"));
    }

    @Test
    void should_return_404_when_staffId_is_not_existed() throws Exception {
        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(status().is(404));
    }

    @Test
    void should_get_null_when_there_is_no_staff() throws Exception {
        mockMvc.perform(get("/api/staffs"))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
    }

    private void addStaff() throws Exception {
        mockMvc.perform(
                post("/api/staffs")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(serialize(new Staff("Rob", "Hall"))));
    }
}
