package com.twuc.webApp.entity;

import javax.persistence.*;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;

@Entity
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String username;

    @Column
    private ZoneId zoneId;

    @Column
    private Instant startTime;

    @Column
    private Duration duration;

    public Reservation(String username, ZoneId zoneId, Instant startTime, Duration duration) {
        this.username = username;
        this.zoneId = zoneId;
        this.startTime = startTime;
        this.duration = duration;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ZoneId getZoneId() {
        return zoneId;
    }

    public void setZoneId(ZoneId zoneId) {
        this.zoneId = zoneId;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }
}
