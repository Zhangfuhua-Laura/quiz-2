package com.twuc.webApp.entity;

public class ZoneId {
    private String zoneId;

    public ZoneId() {
    }

    public ZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }
}
