package com.twuc.webApp.controller;

import com.twuc.webApp.entity.Staff;
import com.twuc.webApp.entity.StaffRepository;
import com.twuc.webApp.entity.ZoneId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.zone.ZoneRulesProvider;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class AddAndGetStaffController {

    @Autowired
    private StaffRepository staffRepository;

    @PostMapping("/staffs")
    ResponseEntity addStaff(@RequestBody Staff staff) {
        try {
            Staff staffSaved = staffRepository.save(staff);
            return ResponseEntity.status(201).header("location", "/api/staffs/" + staffSaved.getId()).build();
        } catch (Exception e) {
            return ResponseEntity.status(400).build();
        }
    }

    @GetMapping("/staffs/{id}")
    ResponseEntity<Staff> getStaffById(@PathVariable Long id) {
        Optional<Staff> staffFound = staffRepository.findById(id);
        if (staffFound.isPresent())
            return ResponseEntity.ok(staffFound.get());
        else
            return ResponseEntity.status(404).build();
    }

    @GetMapping("/staffs")
    ResponseEntity<List<Staff>> getAllStaffs(){
        List<Staff> allStaffs = staffRepository.findAllOrOrderBy();
        return ResponseEntity.ok(allStaffs);
    }

    @PutMapping("/staffs/{id}/timezone")
    ResponseEntity updateTimeZone(@PathVariable Long id, @RequestBody ZoneId timeZone){
        boolean timeZoneIsExist = ZoneRulesProvider.getAvailableZoneIds().contains(timeZone.getZoneId());

        if (timeZoneIsExist){
            Optional<Staff> staffFound = staffRepository.findById(id);
            staffFound.ifPresent(staff -> staff.setTimeZone(timeZone.getZoneId()));
            staffRepository.flush();
            return ResponseEntity.status(200).build();
        }
        else
            return ResponseEntity.status(400).build();
    }

    @GetMapping("/timezones")
    ResponseEntity getTimeZones(){
        return ResponseEntity.ok(ZoneRulesProvider.getAvailableZoneIds());
    }
}
